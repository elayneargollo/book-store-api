using Solutis.Model;
using System.Collections.Generic;
using Solutis.Data.VO;
namespace Solutis.Business
{
    public interface IUserBusiness
    {
        UserVO Validate(string username, string password);
        UserVO Create(UserVO user);
        UserVO FindByID(long id);
        List<UserVO> FindAll();
        UserVO Update(UserVO user);
        void Delete(long id);
    }
}