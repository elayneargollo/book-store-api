using System.Collections.Generic;
using Solutis.Model;
using Solutis.Repository;
using Solutis.Data.VO;
using Solutis.Data.Converter.Implementations;
using Solutis.Services;
using System.Linq;

namespace Solutis.Business.Implementations
{
    public class UserBusinessImplementation : IUserBusiness
    {
        private readonly IRepository<User> _repository;
        private readonly UserConverter _converter;

        public UserBusinessImplementation(IRepository<User> repository)
        {
            _repository = repository;
             _converter = new UserConverter();
        }

        public UserVO Validate(string username, string password)
        {

            var users = ( _converter.Parse(_repository.FindAll()));
            string passwordGenerate =  SecurityService.GenerateMD5(password);

            return users.Where(
                    x =>
                         x.Username.ToLower() == username.ToLower() &&
                         x.Password == passwordGenerate
                         ).FirstOrDefault();
        }


        public List<UserVO> FindAll()
        {
            return  _converter.Parse(_repository.FindAll());
        }

        public UserVO FindByID(long id)
        {
            return  _converter.Parse(_repository.FindById(id));
        }

        public UserVO Create(UserVO user)
        {
            user.Password =  SecurityService.GenerateMD5(user.Password);
            var userEntity = _converter.Parse(user);

            userEntity = _repository.Create(userEntity);
            return  _converter.Parse(userEntity);
        }

        public UserVO Update(UserVO user)
        {
            var userEntity = _converter.Parse(user);

            userEntity = _repository.Update(userEntity);
            return _converter.Parse(userEntity);
        }
        public void Delete(long id)
        {
            _repository.Delete(id);
        }
    }
}