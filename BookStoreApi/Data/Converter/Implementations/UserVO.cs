using Solutis.Data.VO;
using Solutis.Data.Converter.Contract;
using Solutis.Model;
using System.Collections.Generic;
using System.Linq;

namespace Solutis.Data.Converter.Implementations
{
    public class UserConverter : IParser<UserVO, User>, IParser<User, UserVO>
    {
        public User Parse(UserVO origin)
        {
            if (origin == null) return null;
            return new User
            {
                Username = origin.Username,
                Password = origin.Password,
                Role = origin.Role
            };
        }

        public UserVO Parse(User origin)
        {
            if (origin == null) return null;

            return new UserVO
            {
                Username = origin.Username,
                Password = origin.Password,
                Role = origin.Role

            };
        }

        public List<User> Parse(List<UserVO> origin)
        {
            if (origin == null) return null;
            return origin.Select(item => Parse(item)).ToList();
        }


        public List<UserVO> Parse(List<User> origin)
        {
            if (origin == null) return null;
            return origin.Select(item => Parse(item)).ToList();

        }

    }
}