using Newtonsoft.Json;

namespace Solutis.Data.VO
{
    public class UserVO
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

    }
}